const { Router } = require('express');
const UserController = require('../controllers/UserController');
const CommentController = require('../controllers/CommentController');
const router = Router();

router.get('/user',UserController.index);
router.get('/user/:id',UserController.show);
router.post('/user',UserController.create);
router.put('/user/:id', UserController.update);
router.delete('/user/:id', UserController.destroy);

router.get('/comment',CommentController.index);
router.get('/comment/:id',CommentController.show);
router.post('/comment',CommentController.create);
router.put('/comment/:id', CommentController.update);
router.delete('/comment/:id', CommentController.destroy);

module.exports = router;




